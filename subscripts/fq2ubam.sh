#!/bin/bash
#$ -V
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=12G
#$ -e logs/
#$ -o logs/
#$ -t 1-98
#$ -tc 8

config=$1
source $config
set -exou pipefail

IFS=$'\t' read -r SAMPLE LIBRARY RUN FQ1 FQ2 <<<"$(awk "NR == $SGE_TASK_ID" pairs.txt)"
OUTDIR=alignments/${SAMPLE}/ubam
mkdir -p $OUTDIR
PREFIX=${OUTDIR}/${SAMPLE}_${RUN}

echo "SAMPLE = $SAMPLE"
echo "LIBRARY = $LIBRARY"
echo "RUN = $RUN"
echo "FQ1 = $FQ1"
echo "FQ2 = $FQ2"
echo "OUTPUT PREFIX = $PREFIX"

# based on https://gatk.broadinstitute.org/hc/en-us/articles/360039568932
echo $(date) Running Converting to uBAM
$GATK --java-options "-Xmx8G" FastqToSam \
       -F1 ${FQ1} \
       -F2 ${FQ2} \
       -O ${PREFIX}.pre_alignment.bam \
       -SM ${SAMPLE} \
       -RG ${RUN} \
       -PLATFORM illumina \
       -LIBRARY_NAME ${LIBRARY} \
       -TMP_DIR tmp

echo $(date) Marking Illumina adaptors
$GATK --java-options "-Xmx8G" MarkIlluminaAdapters \
    -I ${PREFIX}.pre_alignment.bam \
    -O ${PREFIX}.pre_alignment.markilluminaadapters.bam \
    -M ${PREFIX}.pre_alignment.markilluminaadapters.metrics \
    -ADAPTERS NEXTERA_V1 \
    -ADAPTERS NEXTERA_V2 \
    -TMP_DIR tmp

echo $(date) Done
echo $?
