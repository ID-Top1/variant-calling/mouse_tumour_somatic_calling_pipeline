#!/bin/bash
#$ -M david.parry@igmm.ed.ac.uk
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G
config=$1
source $config

set -exou pipefail

interval=$(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | \
    perl -wane 'print "$F[0]:1-$F[1]\n";' | \
    awk "NR == $SGE_TASK_ID")

gene_db=mutect2_calls/normal_germline_variant_calls/genomics-db/${SGE_TASK_ID}
if [ -d "$gene_db" ]
then
    echo $(date) Found pre-existing gene-db directory $gene_db - removing
    rm -r $gene_db
fi
echo $(date) Creating gene-db directory $gene_db
mkdir -p $(dirname $gene_db)
if [ ! -e mutect2_calls/normal_germline_variant_calls/samplemap.txt ]
then
    touch mutect2_calls/normal_germline_variant_calls/samplemap.txt
    for gvcf in mutect2_calls/normal_gvcfs/*.g.vcf.gz
    do
        sample=$(basename $gvcf .g.vcf.gz)
        echo -e "$sample\t$gvcf"
    done > mutect2_calls/normal_germline_variant_calls/samplemap.txt
fi

echo $(date) Performing GenomicsDBImport for $interval
$GATK --java-options "-Xmx4G" GenomicsDBImport \
    -R $REF \
    --genomicsdb-workspace-path $gene_db \
    --batch-size 50 \
    --sample-name-map mutect2_calls/normal_germline_variant_calls/samplemap.txt \
    -L $interval \
    --tmp-dir tmp

echo $(date) Done
echo $?
