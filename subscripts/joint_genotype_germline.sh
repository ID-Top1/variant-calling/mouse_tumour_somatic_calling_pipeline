#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=8G

config=$1
source $config

set -exou pipefail
mkdir -p mutect2_calls/normal_germline_variant_calls/vcfs
gene_db=mutect2_calls/normal_germline_variant_calls/genomics-db/${SGE_TASK_ID}
vcf=mutect2_calls/normal_germline_variant_calls/vcfs/var.normal_panel.${SGE_TASK_ID}.raw.vcf.gz
interval=$(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | \
    perl -wane 'print "$F[0]:1-$F[1]\n";' | \
    awk "NR == $SGE_TASK_ID")
echo $(date) Genotyping for $interval
$GATK --java-options "-Xmx4G" GenotypeGVCFs \
    -R $REF \
    -V gendb://${gene_db} \
    -O ${vcf}
    
echo $(date) Done
echo $?

