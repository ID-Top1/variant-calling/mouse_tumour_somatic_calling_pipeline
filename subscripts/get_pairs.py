#!/usr/bin/env python
import sys
import os
import glob
from collections import defaultdict


def fq2pairs(rundir):
    fastqs = glob.glob("{}/*/*.fastq.gz".format(rundir))
    if not fastqs:
        raise ValueError("No FASTQs found in subdirectories of " +
                         "{}".format(rundir))
    lib2samp = dict()
    lib2pairs = defaultdict(dict)
    for fq in fastqs:
        samp = os.path.basename(os.path.dirname(fq))
        date, machine, run, flowcell, lane, library, read = \
                os.path.basename(fq).replace('.fastq.gz', '').split("_")
        if library in lib2samp:
            if lib2samp[library] != samp:
                raise ValueError("Found same library for two different " +
                                 "samples  - library {}: samples {} and {}"
                                 .format(library, lib2samp[library], samp))
        else:
            lib2samp[library] = samp
        run_key = "_".join((date, machine, run, flowcell, lane, library))
        if run_key not in lib2pairs[library]:
            lib2pairs[library][run_key] = dict()
        if read in lib2pairs[library][run_key]:
            raise ValueError("Duplicate run and read for FASTQs {} and {}"
                             .format(fq, lib2pairs[library][run_key]))
        lib2pairs[library][run_key][read] = fq
    for lib, run_dict in lib2pairs.items():
        for run, read_dict in run_dict.items():
            for read in ('1', '2'):
                if read not in read_dict:
                    raise ValueError("Missing read {} ".format(read) +
                                     "for sample {}, library {}".format(
                                         lib2samp[lib], lib))
            print("\t".join((lib2samp[lib],
                             lib,
                             run,
                             read_dict['1'],
                             read_dict['2'])))




if __name__ == '__main__':
    if len(sys.argv) != 2:
        sys.exit("Usage: {} <run_directory>".format(sys.argv[0]))
    fq2pairs(sys.argv[1])
