#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=4:00:00
#$ -l h_vmem=2G
# Configure modules
. /etc/profile.d/modules.sh
#Load modules
module load igmm/apps/bcftools/1.10.2

set -exou pipefail

mkdir -p tmp mutect2_calls/panel_of_normals
echo $(date) Concatanating and sorting VCFs
bcftools merge -O u mutect2_calls/normal_sample_vcfs/*vcf.gz | \
    bcftools sort -T tmp/ -O z \
    -o mutect2_calls/panel_of_normals/var.pon.vcf.gz

echo $(date) Indexing
tabix -p vcf mutect2_calls/panel_of_normals/var.pon.vcf.gz

echo $(date) Done
echo $?
