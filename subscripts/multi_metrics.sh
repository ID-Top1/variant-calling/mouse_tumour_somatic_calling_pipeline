#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=48:00:00
#$ -l h_vmem=8G
#$ -t 1-18

config=$1
source $config

set -exou pipefail
SAMPLE=$(awk "NR == $SGE_TASK_ID" <(ls alignments | grep -v merged))
BAM=alignments/merged/${SAMPLE}.mkdups.bqsr.bam
OUTDIR=qc/metrics/multimetrics
PREFIX=${OUTDIR}/${SAMPLE}
mkdir -p $OUTDIR

echo $(date) Running multimetrics on $BAM
$GATK --java-options "-Xmx4G" CollectMultipleMetrics \
    -I $BAM \
    -R $REF \
    -O ${PREFIX} \
    --DB_SNP $KNOWN_SITES \
    --INTERVALS $CHROM_BED \
    --TMP_DIR tmp

echo $(date) Done
echo $?
