#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=12:00:00
#$ -l h_vmem=4G

config=$1
source $config

set -exou pipefail
EXDIR=qc/somalier/extracted
GROUPS=qc/somalier/groups.csv

cut -f 1-2 $TUMOUR_NORMAL_PAIRS | tr '\t' ',' > $GROUPS
echo $(date) Running somalier relate
$SOMALIER relate $EXDIR \
    -g $GROUPS \
    -o qc/somalier/somalier \
    ${EXDIR}/*.somalier

echo $(date) Done
echo $?
