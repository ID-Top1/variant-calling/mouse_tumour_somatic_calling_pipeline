#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=4G

config=$1
source $config
set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"
normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
tumour_bam=alignments/merged/${normal}.mkdups.bqsr.bam
mkdir -p callable_regions/dp30bq20/$normal
mkdir -p callable_regions/dp30bq20/$tumour
mkdir -p callable_regions/dp50bq20/$tumour

echo $(date) Processing $normal at depth 30
./tools/goleft_linux64 depth -q 20 --mincov 30 -r $REF \
    --prefix callable_regions/dp30bq20/$normal/$normal \
    $normal_bam
echo $(date) Compressing
for bed in callable_regions/dp30bq20/${normal}/${normal}*bed
do
    bgzip $bed
    tabix -p bed ${bed}.gz
done

echo $(date) Processing $tumour at depthp 30
./tools/goleft_linux64 depth -q 20 --mincov 30 -r $REF \
    --prefix callable_regions/dp30bq20/$tumour/$tumour \
    $tumour_bam
for bed in callable_regions/dp30bq20/${tumour}/${tumour}*bed
do
    bgzip $bed
    tabix -p bed ${bed}.gz
done

echo $(date) Processing $tumour at depth 50
./tools/goleft_linux64 depth -q 20 --mincov 50 -r $REF \
    --prefix callable_regions/dp50bq20/${tumour}/${tumour} \
    $tumour_bam
for bed in callable_regions/dp50bq20/${tumour}/${tumour}*bed
do
    bgzip $bed
    tabix -p bed ${bed}.gz
done

echo $(date) Done
echo $?
