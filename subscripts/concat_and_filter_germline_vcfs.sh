#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=8G
#$ -l h_rt=4:00:00
# Configure modules
. /etc/profile.d/modules.sh
#Load modules

config=$1
source $config

set -exou pipefail

mkdir -p tmp
prefix=mutect2_calls/normal_germline_variant_calls/var.normal_panel
echo $(date) Concatanating and sorting VCFs
bcftools concat -O u mutect2_calls/normal_germline_variant_calls/vcfs/*vcf.gz | \
    bcftools sort -T tmp/ -O z -o ${prefix}.raw.vcf.gz

echo $(date) Indexing
tabix -p vcf ${prefix}.raw.vcf.gz

echo $(date) Removing original split VCF and genomics-db directories
rm -r mutect2_calls/normal_variant_calls/vcfs mutect2_calls/normal_variant_calls/genomics-db

echo $(date) Performing hard filtering
echo $(date) Selecting SNVs/MNVs
$GATK --java-options "-Xmx4G" SelectVariants \
    -R $REF \
    -V ${prefix}.raw.vcf.gz \
    -select-type SNP \
    -select-type MNP \
    -O ${prefix}.raw.snvs.vcf.gz

echo $(date) Adding hard-filters for SNVs/MNVs
$GATK --java-options "-Xmx4G" VariantFiltration \
    -R $REF \
    -V ${prefix}.raw.snvs.vcf.gz \
    -O ${prefix}.filters.snvs.vcf.gz \
    --filter-expression "QD < 2.0" --filter-name "QDfilter" \
    --filter-expression "FS > 60.0" --filter-name "FSfilter" \
    --filter-expression "MQ < 40.0" --filter-name "MQfilter" \
    --filter-expression "MQRankSum < -12.5" --filter-name "MQRankSumfilter" \
    --filter-expression "ReadPosRankSum < -8.0" --filter-name "ReadPosfilter"

echo $(date) Selecting Indels
$GATK --java-options "-Xmx4G" SelectVariants \
    -R $REF \
    -V ${prefix}.raw.vcf.gz \
    -select-type INDEL \
    -select-type MIXED \
    -O ${prefix}.raw.indels.vcf.gz

echo $(date) Adding hard-filters for indels
$GATK --java-options "-Xmx4G" VariantFiltration \
    -R $REF \
    -V ${prefix}.raw.indels.vcf.gz \
    -O ${prefix}.filters.indels.vcf.gz \
    --filter-expression "QD < 2.0" --filter-name "QDfilter" \
    --filter-expression "FS > 200.0" --filter-name "FSfilter" \
    --filter-expression "ReadPosRankSum < -20.0" --filter-name "ReadPosfilter"

echo $(date) Re-combining filtered variants
bcftools concat -a -O b -o ${prefix}.filters.bcf \
    ${prefix}.filters.snvs.vcf.gz ${prefix}.filters.indels.vcf.gz

echo $(date) Indexing
bcftools index ${prefix}.filters.bcf

echo $(date) Removing intermediate VCFs
rm ${prefix}.filters.snvs.vcf.gz{,.tbi} ${prefix}.filters.indels.vcf.gz{,.tbi} \
    ${prefix}.raw.snvs.vcf.gz{,.tbi}  ${prefix}.raw.indels.vcf.gz{,.tbi} 

echo $(date) Selecting biallelic PASS variants
bcftools view -m2 -M2 --min-ac 2 -f PASS \
    -O z -o ${prefix}.filters.biallelic.pass.min2ac.vcf.gz \
    ${prefix}.filters.bcf

echo $(date) Indexing
tabix -p vcf ${prefix}.filters.biallelic.pass.min2ac.vcf.gz 

echo $(date) Done
echo $?

