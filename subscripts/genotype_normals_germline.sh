#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G

config=$1
source $config
set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"

normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
mkdir -p mutect2_calls/normal_gvcfs
gvcf=mutect2_calls/normal_gvcfs/$(basename $normal_bam .mkdups.bqsr.bam).g.vcf.gz

echo $(date) Creating GVCF for $normal
$GATK --java-options "-Xmx8G" HaplotypeCaller \
    -R $REF \
    -I $normal_bam \
    -O $gvcf \
    -ERC GVCF \
    -G StandardAnnotation \
    -G AS_StandardAnnotation 

echo $(date) Done - GVCF written to $gvcf
echo $?

