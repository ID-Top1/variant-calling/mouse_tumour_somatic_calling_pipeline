#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=24:00:00
#$ -l h_vmem=12G
#$ -t 1-18
#$ -tc 10

config=$1
source $config
set -exou pipefail

SAMPLE=$(awk "NR == $SGE_TASK_ID" <(ls alignments))
BAMDIR=alignments/${SAMPLE}
OUTDIR=alignments/merged
PREFIX=${OUTDIR}/$SAMPLE

mkdir -p $OUTDIR
MERGE_STRING=''
for BAM in $BAMDIR/*.bam
do
    MERGE_STRING="$MERGE_STRING -I $BAM"
done

echo $(date) Merging BAMs
$GATK --java-options "-Xmx8G" MergeSamFiles \
    $MERGE_STRING \
    -O ${PREFIX}.bam \
    --SORT_ORDER coordinate \
    --CREATE_INDEX true \
    --TMP_DIR tmp

echo $(date) Running picard MarkDuplicates
$GATK --java-options "-Xmx8G" MarkDuplicates \
    I=${PREFIX}.bam \
    O=${PREFIX}.mkdups.bam \
    M=${PREFIX}.mkdups.metrics.txt \
    TMP_DIR=tmp \
    TAGGING_POLICY=All \
    CREATE_INDEX=true

echo $(date) Running BQSR
$GATK --java-options "-Xmx8G" BaseRecalibrator \
    -I ${PREFIX}.mkdups.bam \
    -R $REF \
    --known-sites $KNOWN_SITES \
    -O ${PREFIX}.mkdups.bqsr.table

echo $(date) Applying BQSR
$GATK --java-options "-Xmx8G" ApplyBQSR \
    -I ${PREFIX}.mkdups.bam \
    -R $REF \
    -bqsr-recal-file ${PREFIX}.mkdups.bqsr.table \
    -O ${PREFIX}.mkdups.bqsr.bam

echo $(date) removing intermediate BAMs
rm ${PREFIX}.bam ${PREFIX}.mkdups.bam

echo $(date) Running post-BQSR BQSR
$GATK --java-options "-Xmx8G" BaseRecalibrator \
    -I ${PREFIX}.mkdups.bqsr.bam \
    -R $REF \
    --known-sites $KNOWN_SITES \
    -O ${PREFIX}.mkdups.post_bqsr.table

echo $(date) Running AnalyzeCovariates
$GATK --java-options "-Xmx8G" AnalyzeCovariates \
    -before ${PREFIX}.mkdups.bqsr.table \
    -after ${PREFIX}.mkdups.post_bqsr.table \
    -plots ${PREFIX}.mkdups.bqsr.covariates.pdf

echo $(date) Done
echo $?
