#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=48:00:00
#$ -l h_vmem=4G
#$ -pe sharedmem 4

config=$1
source $config
set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"
tumour_bam=alignments/merged/${tumour}.mkdups.bqsr.bam
normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
out_dir=svaba_somatic_indels/${sample}

mkdir -p $out_dir

$SVABA run -t $tumour_bam \
    -n $normal_bam \
    -G $REF \
    -p $NSLOTS \
    -D $KNOWN_INDELS \
    --germline-sv-database $KNOWN_SVS \
    -a ${out_dir}/${sample} \
    -z

echo $(date) Done
echo $?
