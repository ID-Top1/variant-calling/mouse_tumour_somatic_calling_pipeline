#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G

config=$1
source $config

set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"

normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
tumour_bam=alignments/merged/${tumour}.mkdups.bqsr.bam
out_dir=mutect2_calls/mutect2_somatic_calls/${sample}
mutect_vcf=${out_dir}/${sample}.somatic.vcf.gz
filtered_vcf=${out_dir}/${sample}.somatic.filtered.vcf.gz
no_contam_filtered_vcf=${out_dir}/${sample}.somatic.filtered_no_contam.vcf.gz
pileup_dir=mutect2_calls/pileup_summaries
contam_dir=mutect2_calls/contamination_tables
mkdir -p $out_dir $pileup_dir $contam_dir

echo $(date) Running Mutect2 for $sample
$GATK --java-options "-Xmx8G" Mutect2 \
    -R $REF \
    -I $normal_bam \
    -I $tumour_bam \
    -normal $normal \
    --panel-of-normals mutect2_calls/panel_of_normals/var.pon.vcf.gz \
    -O $mutect_vcf

echo $(date) Getting pileup summaries for $tumour
$GATK --java-options "-Xmx8G" GetPileupSummaries \
    -R $REF \
    -I $tumour_bam \
    -V mutect2_calls/normal_germline_variant_calls/var.normal_panel.filters.biallelic.pass.min2ac.vcf.gz \
    -L mutect2_calls/normal_germline_variant_calls/var.normal_panel.filters.biallelic.pass.min2ac.vcf.gz \
    -O ${pileup_dir}/${tumour}.pileupsummaries.table

echo $(date) Getting pileup summaries for $normal
$GATK --java-options "-Xmx8G" GetPileupSummaries \
    -R $REF \
    -I $normal_bam \
    -V mutect2_calls/normal_germline_variant_calls/var.normal_panel.filters.biallelic.pass.min2ac.vcf.gz \
    -L mutect2_calls/normal_germline_variant_calls/var.normal_panel.filters.biallelic.pass.min2ac.vcf.gz \
    -O ${pileup_dir}/${normal}.pileupsummaries.table

echo $(date) Calculating contamination
$GATK --java-options "-Xmx8G" CalculateContamination \
   -I ${pileup_dir}/${tumour}.pileupsummaries.table \
   -matched ${pileup_dir}/${normal}.pileupsummaries.table \
   -O ${contam_dir}/${tumour}.contamination.table

echo $(date) Filtering Mutect calls
$GATK --java-options "-Xmx8G" FilterMutectCalls \
    -R $REF \
    -V $mutect_vcf \
    --contamination-table ${contam_dir}/${tumour}.contamination.table \
    -O $filtered_vcf

echo $(date) Filtering Mutect calls without contamination table
$GATK --java-options "-Xmx8G" FilterMutectCalls \
    -R $REF \
    -V $mutect_vcf \
    --contamination-table ${contam_dir}/${tumour}.contamination.table \
    -O $no_contam_filtered_vcf

echo $(date) Done 
echo $?
