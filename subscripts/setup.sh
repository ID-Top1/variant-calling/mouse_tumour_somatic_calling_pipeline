#!/bin/bash
#$ -V
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=2G
# Configure modules
. /etc/profile.d/modules.sh

config=$1
source $config
set -eou pipefail

mkdir -p tools

gatk_dir=tools/gatk-${GATK_VERSION}
if [ -d $gatk_dir ]
then
    echo $gatk_dir already exists, nothing to do
else
    cd tools
    echo $(date) Getting GATK v${GATK_VERSION}
    wget https://github.com/broadinstitute/gatk/releases/download/${GATK_VERSION}/gatk-${GATK_VERSION}.zip
    unzip -o gatk-${GATK_VERSION}.zip
    rm gatk-${GATK_VERSION}.zip
    cd ..
fi

if [ -e tools/goleft_linux64 ]
then
    echo goleft already present, nothing to do
else
    cd tools 
    wget https://github.com/brentp/goleft/releases/download/v0.2.4/goleft_linux64
    chmod +x goleft_linux64
    cd ../
fi

if [ -d $STRELKA_DIR ]
then
    echo $STRELKA_DIR already exists, nothing to do
else
    cd tools
    echo $(date) Getting Strelka v${STRELKA_VERSION}
    wget https://github.com/Illumina/strelka/releases/download/v${STRELKA_VERSION}/strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    tar xvf strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    rm strelka-${STRELKA_VERSION}.centos6_x86_64.tar.bz2
    cd ..
fi


if [ -d $MANTA_DIR ]
then
    echo $MANTA_DIR already exists, nothing to do
else
    cd tools
    echo $(date) Getting Manta v${MANTA_VERSION}
    wget https://github.com/Illumina/manta/releases/download/v${MANTA_VERSION}/manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    tar xvf manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    rm manta-${MANTA_VERSION}.centos6_x86_64.tar.bz2
    cd ..
fi

if [ -x $SVABA ]
then
    echo $SVABA already exists, nothing to do
else
    cd tools
    echo $(date) Getting svaba
    git clone --recursive https://github.com/walaj/svaba
    cd svaba
    ./configure
    make
    make install
    cd ../../
fi

if [ -x $PLATYPUS ]
then
    echo $PLATYPUS already exists, nothing to do
else
    cd tools
    echo $(date) Getting Platypus
    wget https://www.rdm.ox.ac.uk/files/research/lunter-group/platypus-latest.tgz
    tar xvf platypus-latest.tgz
    rm platypus-latest.tgz
    mv Platypus_*/ Platypus
    cd Platypus
    echo $(date) Building Platypus
    ./buildPlatypus.sh
    cd ../../
fi

if [ -x $SOMALIER ]
then
    echo $SOMALIER already exists, nothing to do
else
    cd tools
    echo $(date) Getting somalier
    wget https://github.com/brentp/somalier/releases/download/v0.2.12/somalier
    chmod +x somalier
    cd ../
fi

echo $(date) Getting GRCm38 reference data
wget -c --no-parent -r -l 1 -A 'GRCm38_68.fa*' -R '*fa.gz' wget ftp://ftp-mouse.sanger.ac.uk/ref/

echo $(date) Moving to ./ref directory
mv -v ftp-mouse.sanger.ac.uk/ref ./
rmdir ftp-mouse.sanger.ac.uk
cd ref 
ln -s GRCm38_68.fa.dict GRCm38_68.dict  # GATK BQSR fails if named with .fa.dict extension
mkdir per_chrom_Fa

echo $(date) Getting known variants
wget $KNOWN_VARS_URL
wget ${KNOWN_VARS_URL}.tbi
wget $KNOWN_INDELS_URL
wget ${KNOWN_INDELS_URL}.tbi
wget $KNOWN_SV_INS_URL
wget ${KNOWN_SV_INS_URL}.tbi
wget $KNOWN_SV_DEL_URL
wget ${KNOWN_SV_DEL_URL}.tbi
zgrep -hv $(basename $KNOWN_SV_INS_URL) $(basename $KNOWN_SV_DEL_URL) \
    sort -k 1,1 -k 2,2n -k 3,3n | bgzip -c > $KNOWN_SVS
cd ..

echo $(date) Removing genotype information from known variants file
bcftools view -G -O z -o $KNOWN_SITES $KNOWN_VARS

echo $(date) Indexing
bcftools index -t $KNOWN_SITES

echo $(date) Creating main chromosome intervals
grep -wP '^(chr)?[0-9XY]+' ${REF}.fai | \
    perl -wane 'print "$F[0]\t0\t$F[1]\n";' > $CHROM_BED

$GATK BedToIntervalList \
    -I $CHROM_BED \
    -O $CHROM_INTERVALS \
    -SD GRCm38_68.dict 


echo $(date) Done
