#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=48:00:00
#$ -l h_vmem=8G
#$ -pe sharedmem 4

config=$1
source $config
set -exou pipefail
out_dir=platypus_output
vcf_out=${out_dir}/all_samples.platypus.vcf
bam_list=${out_dir}/bams.txt

mkdir -p $out_dir
ls alignments/merged/*bqsr.bam > $bam_list

echo $(date) Running Platypus for all samples
python $PLATYPUS callVariants \
    --nCPU=$NSLOTS \
    --logFileName=${out_dir}/log.txt \
    --refFile=$REF \
    --bamFiles=$bam_list \
    --output=${vcf_out}

echo $(date) Compressing and indexing output
bgzip ${vcf_out}
tabix -p vcf ${vcf_out}.gz

echo $(date) Done
echo $?
