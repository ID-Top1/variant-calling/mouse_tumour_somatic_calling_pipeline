#!/bin/bash
#$ -V
#$ -cwd
#$ -l h_rt=24:00:00
#$ -l h_vmem=6G
#$ -e logs/
#$ -o logs/
#$ -t 1-98
#$ -tc 6
#$ -pe sharedmem 8
config=$1
source $config
set -exou pipefail

IFS=$'\t' read -r SAMPLE LIBRARY RUN FQ1 FQ2 <<<"$(awk "NR == $SGE_TASK_ID" pairs.txt)"
OUTDIR=alignments/${SAMPLE}
mkdir -p $OUTDIR
OUT_BAM=${OUTDIR}/${SAMPLE}_${RUN}.bam
INDIR=alignments/${SAMPLE}/ubam
IN_BAM=${INDIR}/${SAMPLE}_${RUN}.pre_alignment.markilluminaadapters.bam
U_BAM=${INDIR}/${SAMPLE}_${RUN}.pre_alignment.bam
BWA_THREADS=$(expr $NSLOTS - 2)

echo "SAMPLE = $SAMPLE"
echo "LIBRARY = $LIBRARY"
echo "RUN = $RUN"
echo "INPUT uBAM = $IN_BAM"
echo "OUTPUT BAM = $OUT_BAM"

# based on https://gatk.broadinstitute.org/hc/en-us/articles/360039568932
echo $(date) Running bwa and merging unmapped alignments
$GATK --java-options "-Xmx8G" SamToFastq \
    -I $IN_BAM \
    --FASTQ /dev/stdout \
    --CLIPPING_ATTRIBUTE XT --CLIPPING_ACTION 2 --INTERLEAVE true --NON_PF true \
    --TMP_DIR tmp | \
bwa mem -M -t $BWA_THREADS -p $REF \
    -R "@RG\tID:$RUN\tSM:$SAMPLE\tLB:$LIBRARY\tPL:ILLUMINA" \
    /dev/stdin | \
$GATK --java-options "-Xmx16G" MergeBamAlignment \
    -ALIGNED_BAM /dev/stdin \
    -UNMAPPED_BAM $U_BAM \
    -OUTPUT $OUT_BAM \
    -R $REF \
    --CREATE_INDEX true --ADD_MATE_CIGAR true \
    --CLIP_ADAPTERS false --CLIP_OVERLAPPING_READS true \
    --INCLUDE_SECONDARY_ALIGNMENTS true --MAX_INSERTIONS_OR_DELETIONS -1 \
    --PRIMARY_ALIGNMENT_STRATEGY MostDistant --ATTRIBUTES_TO_RETAIN XS \
    --TMP_DIR tmp

echo $(date) Removing pre-alignment BAMs
rm $IN_BAM $U_BAM

echo $(date) Done
echo $?
