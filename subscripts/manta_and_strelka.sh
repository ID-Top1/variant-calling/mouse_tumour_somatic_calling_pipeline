#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=48:00:00
#$ -l h_vmem=2G
#$ -pe sharedmem 8

config=$1
source $config
set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"
tumour_bam=alignments/merged/${tumour}.mkdups.bqsr.bam
normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
manta_out=strelka_somatic_calls/manta_calls/${sample}
strelka_out=strelka_somatic_calls/strelka_calls/${sample}

mkdir -p $manta_out $strelka_out

call_regions=strelka_somatic_calls/strelka_calls/${sample}/call_regions.bed.gz
grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | \
    perl -wane 'print "$F[0]\t0\t$F[1]\n";' | \
    bgzip -c > $call_regions
tabix -p bed $call_regions

echo $(date) - running configureManta.py
python2 $MANTA_DIR/bin/configManta.py \
    --normalBam $normal_bam \
    --tumorBam $tumour_bam \
    --referenceFasta $REF \
    --runDir $manta_out \
    --callRegions $call_regions

echo $(date) - running manta workflow
python2 ${manta_out}/runWorkflow.py -m local -g 16 -j 8

echo $(date) - running configureStrelkaSomaticWorkflow.py
python2 $STRELKA_DIR/bin/configureStrelkaSomaticWorkflow.py  \
    --referenceFasta $REF \
    --normalBam $normal_bam \
    --tumorBam $tumour_bam \
    --indelCandidates ${manta_out}/results/variants/candidateSmallIndels.vcf.gz \
    --outputCallableRegions \
    --runDir $strelka_out \
    --callRegions $call_regions

echo $(date) - running strelka workflow
python2 ${strelka_out}/runWorkflow.py -m local -g 16 -j 8

echo $(date) - finished manta/strelka workflow
echo $?
