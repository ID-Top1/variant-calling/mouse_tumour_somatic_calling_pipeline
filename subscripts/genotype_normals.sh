#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=8G

config=$1
source $config
set -exou pipefail
read -r tumour normal sample <<<"$(awk "NR == $SGE_TASK_ID" $TUMOUR_NORMAL_PAIRS)"

normal_bam=alignments/merged/${normal}.mkdups.bqsr.bam
mkdir -p mutect2_calls/normal_sample_vcfs
vcf=mutect2_calls/normal_sample_vcfs/$(basename $normal_bam .mkdups.bqsr.bam).vcf.gz

echo $(date) Creating GVCF for $normal
$GATK --java-options "-Xmx4G" Mutect2 \
    -R $REF \
    -I $normal_bam \
    --max-mnp-distance 0 \
    -O $vcf

echo $(date) Done - VCF written to $vcf
echo $?
