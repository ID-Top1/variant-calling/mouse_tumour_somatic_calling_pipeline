#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=24:00:00
#$ -l h_vmem=4G
#$ -t 1-18
module load igmm/apps/FastQC/0.11.9

set -exou pipefail
IFS=$'\t' read -r SAMPLE LIBRARY RUN FQ1 FQ2 <<<"$(awk "NR == $SGE_TASK_ID" pairs.txt)"

OUTDIR=qc/fastqc/${SAMPLE}
mkdir -p $OUTDIR
echo $(date) Running fastqc for $FQ1 and $FQ2
fastqc -o $OUTDIR $FQ1 $FQ2

echo $(date) Done
echo $?
