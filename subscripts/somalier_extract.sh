#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_rt=12:00:00
#$ -l h_vmem=4G
#$ -t 1-18

config=$1
source $config

set -exou pipefail
SAMPLE=$(awk "NR == $SGE_TASK_ID" <(ls alignments | grep -v merged))
BAM=alignments/merged/${SAMPLE}.mkdups.bqsr.bam
OUTDIR=qc/somalier/extracted
mkdir -p $OUTDIR

echo $(date) Running somalier on $BAM
$SOMALIER extract -d $OUTDIR \
    --sites mutect2_calls/normal_germline_variant_calls/var.normal_panel.filters.biallelic.pass.min2ac.snvs.vcf.gz \
    -f $REF \
    $BAM

echo $(date) Done
echo $?
