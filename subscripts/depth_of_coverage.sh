#!/bin/bash
#$ -V
#$ -cwd
#$ -e logs/
#$ -o logs/
#$ -l h_vmem=12G

config=$1
source $config

set -exou pipefail
bamstring=''
for bam in alignments/merged/*.mkdups.bqsr.bam
do
    bamstring="$bamstring -I $bam"
done

chrom=$(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | \
    cut -f 1 | \
    awk "NR == $SGE_TASK_ID")
mkdir -p depth/${chrom}

echo $(date) Getting depth of coverage genome-wide
$GATK --java-options "-Xmx8G" DepthOfCoverage \
   -R $REF \
   -O depth/${chrom}/depth_${chrom} \
   $bamstring \
   --min-base-quality 20 \
   --summary-coverage-threshold 4 \
   --summary-coverage-threshold 9 \
   --summary-coverage-threshold 19 \
   --summary-coverage-threshold 29 \
   --summary-coverage-threshold 49 \
   -L $chrom

echo $(date) Compressing per-base coverage
bgzip depth/${chrom}/depth_${chrom}

echo $(date) Done
echo $?
