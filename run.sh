#!/bin/bash

set -exou pipefail

mkdir -p logs

source_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
script_dir=${source_dir}/subscripts
config=${source_dir}/config.sh
source $config

${script_dir}/get_pairs.py $FQDIR > pairs.txt

sample_tasks=$(cat $TUMOUR_NORMAL_PAIRS | wc -l) 
gt_tasks=$(grep -wP '^(chr)?[0-9XYMT]+' ${REF}.fai | wc -l )
pindel_tasks=$( expr $sample_tasks '*' $gt_tasks )

# FASTQ to processed alignments
hold=$(qsub -terse ${script_dir}/fq2ubam.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} ${script_dir}/align.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} ${script_dir}/process_alignments.sh $config | cut -f 1 -d.)

# misc. QC
metrics_hold=$(qsub -terse -hold_jid ${hold} ${script_dir}/multi_metrics.sh $config | cut -f 1 -d .)
qsub -hold_jid ${hold} ${script_dir}/wgs_metrics.sh $config
qsub -hold_jid ${hold} ${script_dir}/oxog_metrics.sh $config
qsub -hold_jid ${hold} ${script_dir}/fastqc.sh

# Somatic genotyping with strelka 
qsub -hold_jid ${hold} -t 1-$(cat $TUMOUR_NORMAL_PAIRS | wc -l) ${script_dir}/manta_and_strelka.sh $config

# Joint genotyping with platypus
qsub -hold_jid ${hold} ${script_dir}/platypus.sh $config

# Indel/SV genotyping with svaba
qsub -hold_jid ${hold} -t 1-$(cat $TUMOUR_NORMAL_PAIRS | wc -l) ${script_dir}/svaba.sh $config

# Genotyping with GATK/Mutect2
germline_hold=$(qsub -terse -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/genotype_normals_germline.sh $config | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} -t 1-${gt_tasks} ${script_dir}/import_gvcfs.sh $config | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} -t 1-${gt_tasks} ${script_dir}/joint_genotype_germline.sh $config | cut -f 1 -d.)
germline_hold=$(qsub -terse -hold_jid ${germline_hold} ${script_dir}/concat_and_filter_germline_vcfs.sh $config | cut -f 1 -d.)
somalier_hold=$(qsub -hold_jid ${germline_hold} -t 1-${sample_tasks} ${script_dir}/somalier_extract.sh $config | cut -f 1 -d.)
qsub -hold_jid ${somalier_hold} ${script_dir}/somalier_relate.sh $config

hold=$(qsub -terse -hold_jid ${hold} -t 1-${sample_tasks} ${script_dir}/genotype_normals.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} -t 1-${gt_tasks} ${script_dir}/make_panel_of_normals.sh $config | cut -f 1 -d.)
hold=$(qsub -terse -hold_jid ${hold} ${script_dir}/concat_normal_vcfs.sh $config)

# Mutect2 with panel of normals
qsub -hold_jid ${hold},${germline_hold} -l h_rt=96:00:00 -t 1-${sample_tasks} ${script_dir}/mutect2.sh $config
